use std::io;

struct Square {
    risk: u8,
    path_risk: u32,
    path_prev: usize
}

struct Board {
       squares: Vec<Square>,
       w: usize,
       h: usize
}

impl Board {
    fn new_path_risk(&self, cur: usize, last: usize) -> u32 {
        return self.squares[cur].risk as u32 +
            self.squares[last].path_risk;
    }
    fn set_path(&mut self, cur: usize, last: usize, risk: u32) {
        self.squares[cur].path_risk = risk;
        self.squares[cur].path_prev = last;
    }
    fn set_path_with_risk(&mut self, cur: usize, last: usize) {
        self.set_path(cur, last, self.new_path_risk(cur, last));
    }
    fn set_path_best(&mut self, cur: usize, last1: usize, last2: usize) -> usize {
        let r1 = self.new_path_risk(cur, last1);
        let r2 = self.new_path_risk(cur, last2);

        if r1 < r2 {
            self.set_path(cur, last1, r1);
            return last2;
        } else {
            self.set_path(cur, last2, r2);
            return last1;
        }
    }

    fn square_has_adj_n(&mut self, ind: usize) -> bool {
        ind >= self.w
    }
    fn square_has_adj_w(&mut self, ind: usize) -> bool {
        ind % self.w != 0
    }
}

fn read_with_len(input: &mut String) -> Option<usize> {
    match io::stdin().read_line(input) {
        Ok(_) => {
            *input = input.trim().to_string();
            if input.is_empty() {
                None
            } else {
                Some(input.len())
            }
        }
        Err(e) => match e.kind() {
            io::ErrorKind::UnexpectedEof => None,
            _ => panic!("Failed to read line!")
        }
    }
}

fn add_to_board(input: &mut String, board: &mut Vec<Square>) {
    for b in input.bytes() {
        if b < ('0' as u8) || b > ('9' as u8) {
            panic!("Invalid format!");
        }
        board.push(Square { 
            risk: (b - ('0' as u8)), 
            path_risk: 0,
            path_prev: 0
        });
    }
}

fn parse_board() -> Board {
    let mut input = String::new();
    let len = read_with_len(&mut input).expect("File empty!");
    let mut board: Vec<Square> = Vec::new();
    let mut lines = 1;

    
    loop {
        add_to_board(&mut input, &mut board);

        input.clear();

        match read_with_len(&mut input) {
            Some(n) => {
                if n != len {
                    panic!("Invalid format!");
                }
                lines += 1;
            },
            None => break
        };
    }

    Board {
        squares: board,
        w: len,
        h: lines
    }
}

fn main() {
    let mut board = parse_board();
    let mut diag_rows: Vec<Vec<usize>> = vec![Vec::new(); board.w+board.h-1];
    //ugly amount of memory allocation happening here
    for i in 0..board.h {
        for j in 0..board.w {
            diag_rows[i+j].push(i*board.w+j);
        }
    }

    //not a for loop so the program can rewind it
    let mut i = 1;
    while i < diag_rows.len() {
        let mut should_rewind = false;
        for j in 0..diag_rows[i].len() {
            let ind = diag_rows[i][j];

            if board.square_has_adj_n(ind) {
                if board.square_has_adj_w(ind) { //square exists N, W
                    let other = board.set_path_best(ind, ind - board.w, ind - 1);

                    let new_risk = board.new_path_risk(other, ind);
                    if new_risk < board.squares[other].path_risk {
                        board.set_path(other, ind, new_risk);
                        diag_rows[i].remove(j);
                        i -= 1;
                        should_rewind = true;
                        break;
                    }
                } else { //square exists N
                    board.set_path_with_risk(ind, ind - board.w);
                }
            } else if board.square_has_adj_w(ind) { //square exists W
                board.set_path_with_risk(ind, ind - 1);
            }
        }

        if should_rewind {
            while i > 0 {
                should_rewind = false;
                for j in (0..diag_rows[i].len()).rev() {
                    let ind = diag_rows[i][j];
                    let prev = board.squares[ind].path_prev;
                    if prev > ind { //must be E or S
                        if board.square_has_adj_w(ind) {
                            let other = ind - board.w;
                            let new_risk = board.new_path_risk(other, ind);
                            if new_risk < board.squares[other].path_risk {
                                board.set_path(other, ind, new_risk);
                                diag_rows[i].remove(j);
                                should_rewind = true;
                            }
                        }
                        if board.square_has_adj_n(ind) {
                            let other = ind - 1;
                            let new_risk = board.new_path_risk(other, ind);
                            if new_risk < board.squares[other].path_risk {
                                board.set_path(other, ind, new_risk);
                                diag_rows[i].remove(j);
                                should_rewind = true;
                            }
                        }
                    }
                }
                if !should_rewind {
                    break;
                }
                i -= 1;
            }
        }

        i += 1;
    }
    let mut i = board.squares.len() - 1;

    while board.squares[i].path_prev != i {
        i = board.squares[i].path_prev;
    }

    if let Some(last) = board.squares.last() {

    } else {
        println!("this shouldn't happen");
    }
}
