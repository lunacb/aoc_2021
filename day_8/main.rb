#!/usr/bin/env ruby

$o = STDIN.read.gsub(/\|\n/, "| ")

s1 = $o.split("\n").map do |a|
  a.split("|")[1].split
end.flatten.filter { |a| [2,4,3,7].include? a.length }.count

puts "part 1: #{s1}"

numbers = {
    [ 0,1,2,4,5,6 ] => 0,
    [ 0,1,3,4,5,6 ] => 6,
    [ 0,1,2,3,5,6 ] => 9,
    [ 0,2,3,4,6 ] => 2,
    [ 0,2,3,5,6 ] => 3,
    [ 0,1,3,5,6 ] => 5,
    [2,5] => 1,
    [1,2,3,5] => 4,
    [0,2,5] => 7,
    (0..6).to_a => 8,
}

indices = numbers.each_key.chunk { |a| a.length }.to_h.transform_values { |a| a.reduce { |b,c| b & c } }

def eliminate(possible, i, confirmed)
  if possible[i].length == 1 then
    confirmed |= possible[i]
    possible.each_with_index do |a, j|
      if a.length != 1 and a.include? possible[i].first then
        possible[j] = a.difference(possible[i])
        eliminate(possible, j, confirmed)
      end
    end
  end
end

$o = $o.split("\n").reduce(0) do |old,a|
  possible = [('a'.ord..'g'.ord).to_a.map { |b| b.chr }] * 7
  confirmed = []

  while possible.any? { |b| b.length > 1 } do
    a.split("|")[0].split.each do |b|
      if indices[b.length] != nil then
        indices[b.length].each do |c|
          if possible[c].length != 1 then
            possible[c] &= b.chars
            eliminate(possible, c, confirmed)
          end
        end
      end
    end
  end

  map = {}
  possible.each_with_index { |b,i| map[b.first] = i }

  old + a.split("|")[1].split.map { |b| numbers[b.chars.map { |c| map[c] }.sort] }.join.to_i
end

puts "part 2: #{$o}"
